function [m b] = Least_squere_linear( x,y ) %linear parameters meaning: y=mx+b

buffer=0;
x_avg=0;
y_avg=0;
sum_x = 0;
sum_xy=0;
szum_xx=0;

if (length(x) ~= length(y))
    disp('Length of x and y is not the same! Further calculations are not possible.');
    return
end

%sum and avarage of the x array:
for i=1:length(x)    
    buffer = buffer + x(i);    
end
sum_x = buffer;
x_avg=buffer / length(x);
buffer = 0;

%avarage of the y array
for i=1:length(y)    
    buffer = buffer + y(i);    
end
y_avg=buffer / length(y);
buffer = 0;

%sum of the x*y multiplication
for i=1:length(x)    
    buffer = buffer + (x(i)*y(i));    
end
sum_xy = buffer;
buffer = 0;

%sum of the x^2
for i=1:length(x)    
    buffer = buffer + (x(i)*x(i));    
end
sum_xx=buffer;
buffer = 0;

m=((y_avg*sum_x) - sum_xy)/((x_avg*sum_x)-sum_xx);
b=y_avg-(m*x_avg);

fprintf('y=%f *x + %f \n',m,b)

end

