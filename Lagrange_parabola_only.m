function [ yq ] = Lagrange_parabola_only( y,x, xq)

%polynimials initializing
p1=poly(0);
p2=poly(0);
poly_buff=poly(0);
%fill with zeros
poly_buff=zeros(1,4);
%readout the length of input locations
[n,m]=size(x);
%implementing Lagrange method
for i=1:3
    p2=poly(0);
    p2=zeros(1,2);
    p2(2)=1;
    for j=1:3
        if(i~=j)
            p1(1)=1;
            p1(2)=-x(j);
            p1=p1/(x(i)-x(j)); 
            p2=conv(p2,p1);
        end        
    end
        p2=y(i)*p2;
        poly_buff=poly_buff+p2;
    
end
%fill up the global polynomial coefficients array with the calculated
%Lagrange-polynomial's coefficeients
for i=1:4
    polynomial_coeffs(1,i)=poly_buff(i);
end
%calculate the derivatives
derivalt(1)=polyval(polyder(poly_buff),x(3));
derivalt(2)=polyval(polyder(polyder(poly_buff)),x(3));
polyder(poly_buff)
derivalt
poly_buff

%hermite polynomial fitting for every pair of input location
for i=3:(m-1)
    p=Hermite_3th_polinom([y(i),y(i+1)],[x(i),x(i+1)],derivalt);
        for j=1:4
            polynomial_coeffs(i-1,j)=p(j);
        end
        derivalt(1)=p(5);
        derivalt(2)=p(6);
end
polynomial_coeffs
%evaluating 
yq=polyval(polynomial_coeffs(m-2,:),xq);

%here come some plotting, to control the process visually
plot(xq,yq,'cx','LineWidth',10);
hold on;
plot(x,y,'ro');
plot(x(1)-1:0.01:x(3),polyval(poly_buff,x(1)-1:0.01:x(3)));
for k=3:(m-1)    
    for i=x(k):0.001:x(k+1)
           plot(i,polyval(polynomial_coeffs(k-1,:),i));
    end
end

for i=x(m):0.001:(xq+0.5)
    plot(i,polyval(polynomial_coeffs(m-2,:),i));
end

hold off;
legend('extrapolated value','given points','fv');



end
