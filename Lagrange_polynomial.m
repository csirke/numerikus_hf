function [ yq ] = Lagrange_polynomial( y,x,xq ) %y:eredeti y ertek, x:az egyes y ertekek helye, xq: milyen helyen keres�nk
%nem csak extrapol�l�sra alkalmas elj�r�s
eredmeny=0;
szamlalo=1;
nevezo=1;

for i = 1:length(x)
    for j = 1:length(x)
        if i~=j            
        szamlalo=szamlalo * (xq-x(j));
        nevezo=nevezo * (x(i)-x(j));
        end    
    end
        eredmeny = eredmeny + (y(i)*szamlalo/nevezo);
        szamlalo=1;
        nevezo=1;
end
yq = eredmeny;%visszat�r�si �rt�k
end