clc;
clear all;
close all;

% x=[1 2];
% y=[2 3];

%input values
x = [0 1 2 3 4 5 6]; 
y = [0 2 4 6 8 20 150];

%requested location of extrapolated value, initiate the value too
xq = 12;
yq = 0;

[a] = Least_squere_optional_degree_polynom(x,y,4);

k=0;
buff=0;
res = 0;
for i=-10:0.01:20
    for j=1:length(a)
       buff = buff + (a(j,1)*(i^(j-1)));
    end
    k=k+1;
    res(2,k) = i;
    res(1,k) = buff;
    buff =0;
end


k=0;
buff=0;
for j=1:length(a)
   buff = buff + (a(j,1)*(xq^(j-1)));
end
yq=buff;



hold on
plot (x,y,'ro');
plot (res(2,:),res(1,:));
plot (xq,yq,'cx','LineWidth',10);
legend('given values','calculated line','extrapolated value');
hold off