function [ a ] = Least_squere_optional_degree_polynom( x,y,deg )

buff =0;
y_vect =0;
x_vect =0;
a_vect = 0;

if (length(x) ~= length(y))
    disp('Length of x and y is not the same! Further calculations are not possible.');
    return
end

for i=1:deg
    for j=1:length(y)
        buff = buff + (y(j)*(x(j)^(i-1)));
    end
    y_vect(i,1)=buff
    buff =0;
end

buff = 0;

x_vect(1,1)=length(x);

for j=2:deg
    for k=1:length(x)
        buff = buff + (x(k)^(j-1));
    end
    x_vect(1,j) = buff;
    buff = 0;
end


for i=2:deg %sor l�ptet
    for j=1:deg %oszlop l�ptet
        for k=1:length(x)%sum
            buff = buff + (x(k)^((i-2)+(j))); 
        end
        x_vect(i,j) = buff;
        buff = 0;
    end
end

a_vect = inv(x_vect) * y_vect;

a=a_vect;


fprintf('y=\n')
for i=1:deg
    fprintf('%f *x^%d\n',a_vect(i,1),i-1)
end

end

