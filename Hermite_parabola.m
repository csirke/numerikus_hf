function [ yq ] = Hermite_parabola( y,x,y_der,xq )
%mukodesi elve u.a mint a harmadfoku Hermite ivnek
yq=0;

p=poly(0);

%feltoltes
for i=1:2
    for j=0:2
        A(i,3-j)=power(x(i),j);
    end
end
A(3,1)=2*power(x(1),1);
A(3,2)=1;
A(3,3)=0;

A(4,1)=2;
A(4,2)=0;
A(4,3)=0;

B=[y(1);y(2);y_der(1);y_der(2)];


disp(A)
disp(B)
p=linsolve(A,B);
p=p';

 yq=polyval(p,xq);%kiiratas
end
