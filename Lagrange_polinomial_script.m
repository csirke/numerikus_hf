clc;
clear all;

x = [ -2,-1,1,2,3,0]%a kapott pontokat tartalmaz� vektorok
y=[33,100,-56,-100,13,100]

j=0;
for i=-5:0.001:5 %kapott polinom kist�m�t�sa -5-t�l +5-ig, adott l�p�sk�zzel
j=j+1;
    res(1,j) = Lagrange_polynomial(y,x,i);
res(2,j) = i;
end

extralpolated_location=4;  %keresett hely
extrapolated_value=Lagrange_polynomial(y,x,extralpolated_location); %extrapol�ci�

plot(res(2,:),res(1,:)) %innent�l csak plottol�s
hold on
plot(x,y,'ro')
plot(extralpolated_location,extrapolated_value,'cx','LineWidth',10)
legend('kapott polinom','eredeti �rt�kek','extrapol�lt �rt�k');
hold off