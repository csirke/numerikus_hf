function [ yq ] = Hermite_3th_polinom( y,x,y_der )

p=poly(0);

for i=1:2
    for j=0:3
        A(i,4-j)=power(x(i),j);
    end
end
A(3,1)=3*power(x(1),2);
A(3,2)=2*power(x(1),1);
A(3,3)=1;
A(3,4)=0;

A(4,1)=6*power(x(1),1);
A(4,2)=2;
A(4,3)=0;
A(4,4)=0;

B=[y(1);y(2);y_der(1);y_der(2)];

p=linsolve(A,B);
p=p';

yq=[p,polyval(polyder(p),x(2)),polyval(polyder(polyder(p)),x(2))]; %annyi a kul. a Hermite_3th-hoz kepest, hogy itt a polinom egyutthatokat es a derivaltakat adjuk at, nem csak egyetlen erteket

end
