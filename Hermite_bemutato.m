clc;
clear all;

x=[2,5];
y=[1,4];
y_der=[-2, 2];

extralpolated_location=7;  %keresett hely
extrapolated_value=Hermite_parabola(y,x,y_der,extralpolated_location);

plot(0:0.01:8,Hermite_parabola(y,x,y_der,0:0.01:8))
hold on
plot(x,y,'ro');
plot(extralpolated_location,extrapolated_value,'cx','LineWidth',10);
legend('kapott polinom','eredeti �rt�kek:y=(x-3)^2','extrapol�lt �rt�k');
hold off

clc;
clear all;

x=[2,5];
y=[-1,8];
y_der=[3,-6];

extralpolated_location=7;  %keresett hely
extrapolated_value=Hermite_3th(y,x,y_der,extralpolated_location);

figure;

plot(0:0.01:8,Hermite_3th(y,x,y_der,0:0.01:8));
hold on
plot(x,y,'ro');
plot(extralpolated_location,extrapolated_value,'cx','LineWidth',10);
legend('kapott polinom','eredeti �rt�kek:y=(x-3)^3','extrapol�lt �rt�k');
hold off