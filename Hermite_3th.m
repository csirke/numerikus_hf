function [ yq ] = Hermite_3th( y,x,y_der,xq )
%a polinom illesztest sikerut matrixos alakra visszavezetni

p=poly(0);

%innen csak az A es B matrixokat toltjuk fel megfelelo szamokkal
for i=1:2
    for j=0:3
        A(i,4-j)=power(x(i),j);
    end
end
A(3,1)=3*power(x(1),2);
A(3,2)=2*power(x(1),1);
A(3,3)=1;
A(3,4)=0;

A(4,1)=6*power(x(1),1);
A(4,2)=2;
A(4,3)=0;
A(4,4)=0;

B=[y(1);y(2);y_der(1);y_der(2)];

p=linsolve(A,B); %Ap=B egyenlet megoldasa (p-re, ahol x az a,b,c,d egyutthatokat tartalmazza)
p=p';

yq=polyval(p,xq); %csak a kivant erteket adjuk at

end
