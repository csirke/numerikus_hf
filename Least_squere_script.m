clc;
clear all;

%input values
x = [0 1 2 3 4 5 6]; 
y = [0 2 4 6 8 20 150];

%requested location of extrapolated value, initiate the value too
xq = 12;
yq = 0;

%calculation of the best fit linear
[m b] = Least_squere_linear(x,y);

%calculation of the extrapolated value (y = mx +b)
yq = m*xq +b;

%make some point of the linear
j=0;
for i=-10:0.01:20
    j=j+1;
    res(1,j) = m*i + b;
    res(2,j) = i;
end

hold on
plot (x,y,'ro');
plot (res(2,:),res(1,:));
plot (xq,yq,'cx','LineWidth',10);
legend('given values','calculated line','extrapolated value');
hold off